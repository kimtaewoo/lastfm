//
//  LastFmTests.swift
//  LastFm
//
//  Created by ktw on 06.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import XCTest
import Alamofire
import ObjectMapper
import CoreData

class LastFmTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDataService_requestArtistList() {
        let service = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "aa"))
        if let _ = ServiceManager.shared.queryService(service) {
            XCTAssert(true)
        }
        else {
            XCTAssert(false)
        }
    }
    
    func testDataService_requestArtistList_static_sameKeyword() {
        let service00 = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "aa"))
        let service01 = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "aa"))
        let fetchCtrl00 = ServiceManager.shared.queryService(service00)
        let fetchCtrl01 = ServiceManager.shared.queryService(service01)
        
        if fetchCtrl00 == fetchCtrl01 {
            XCTAssert(true)
        }
        else {
            XCTAssert(false)
        }
    }
    
    func testDataService_requestArtistList_static_differentKeyword() {
        let service00 = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "aa"))
        let service01 = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "bb"))
        let fetchCtrl00 = ServiceManager.shared.queryService(service00)
        let fetchCtrl01 = ServiceManager.shared.queryService(service01)
        
        if fetchCtrl00 != fetchCtrl01 {
            XCTAssert(true)
        }
        else {
            XCTAssert(false)
        }
    }
    
    
    func testDataService_emptyArtistList() {
        let service = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: "aa"))
        ServiceManager.shared.emptyService(service)

        let fetchCtrl = ServiceManager.shared.queryService(service)
        do {
            try fetchCtrl?.performFetch()
        } catch {
        }
        
        if nil == fetchCtrl?.fetchedObjects ||
            0 == fetchCtrl?.fetchedObjects?.count {
            XCTAssert(true)
        }
        else {
            XCTAssert(false)
        }
    }
    
    
    func testDataService_emptyAlbumList() {
        let artistKey = ArtistKey(mbid: nil, name: "Black Stone Cherry")
        // let artistKey = ArtistKey(mbid: "1801bd47-46ae-49ff-bfcd-6e01b562d880", name: nil)
        let service = ServiceType.albumList(artistKey: artistKey)
        ServiceManager.shared.emptyService(service)

        let fetchCtrl = ServiceManager.shared.queryService(service)
        do {
            try fetchCtrl?.performFetch()
        } catch {
        }
        
        if nil == fetchCtrl?.fetchedObjects ||
            0 == fetchCtrl?.fetchedObjects?.count {
            XCTAssert(true)
        }
        else {
            XCTAssert(false)
        }
    }
    
    
    func testDataService_requestAlbumInfo() {
        let expectation = self.expectation(description: "expectation: get album info")
        let albumKey = AlbumKey(mbid: "8daf4cd8-def7-4993-b1ef-340700ca95ff", name: "Tattoo", artist: "Rory Gallagher")
        let service = ServiceType.albumInfo(albumKey: albumKey)
        ServiceManager.shared.requestService(service) { fetchedResultsController in
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if nil == error {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        }
    }

    
    func testNetworkService_requestAlbumInfo() {
        let expectation = self.expectation(description: "expectation: get album info")
        let albumKey = AlbumKey(mbid: "8daf4cd8-def7-4993-b1ef-340700ca95ff", name: "Tattoo", artist: "Rory Gallagher")
        let netService = NetworkServiceType.albumInfo(albumKey: albumKey) { albumResponse in
            expectation.fulfill()
        }
        let _ = NetworkService.shared.request(netService)
        
        waitForExpectations(timeout: 10) { error in
            if nil == error {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        }
    }
    
    
    func testNetworkService_OQ_requestAlbumInfo() {
        let expectation = self.expectation(description: "expectation: get album info")
        let albumKey = AlbumKey(mbid: "8daf4cd8-def7-4993-b1ef-340700ca95ff", name: "Tattoo", artist: "Rory Gallagher")
        let netService = NetworkServiceType.albumInfo(albumKey: albumKey) { albumResponse in
            expectation.fulfill()
        }
        let _ = NetworkService.shared.request(netService)
        
        waitForExpectations(timeout: 10) { error in
            if nil == error {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        }
    }
    
    func testNetworkService_OQ_requestAlbumInfo_checkInQueue() {
        let expectation = self.expectation(description: "expectation: get album info")


        if false {
            //        let albumKey00 = AlbumKey(mbid: "8daf4cd8-def7-4993-b1ef-340700ca95f_", name: "Tattoo", artist: "Rory Gallagher")
            //        let albumKey01 = AlbumKey(mbid: "8daf4cd8-def7-4993-b1ef-340700ca95ff", name: "Tattoo", artist: "Rory Gallagher")
            //        let albumKey00 = AlbumKey(mbid: nil, name: "Tattoo", artist: "Rory Gallaghe_")
            //        let albumKey01 = AlbumKey(mbid: nil, name: "Tattoo", artist: "Rory Gallagher")
            let albumKey00 = AlbumKey(mbid: nil, name: "Tatto0", artist: "Rory Gallagher")
            let albumKey01 = AlbumKey(mbid: nil, name: "Tatto1", artist: "Rory Gallagher")
            let albumKey02 = AlbumKey(mbid: nil, name: "Tatto2", artist: "Rory Gallagher")
            let albumKey03 = AlbumKey(mbid: nil, name: "Tatto3", artist: "Rory Gallagher")
            let albumKey04 = AlbumKey(mbid: nil, name: "Tatto4", artist: "Rory Gallagher")
            let albumKey05 = AlbumKey(mbid: nil, name: "Tatto5", artist: "Rory Gallagher")
            let albumKey06 = AlbumKey(mbid: nil, name: "Tatto6", artist: "Rory Gallagher")
            let albumKey07 = AlbumKey(mbid: nil, name: "Tatto7", artist: "Rory Gallagher")
            let albumKey08 = AlbumKey(mbid: nil, name: "Tatto8", artist: "Rory Gallagher")
            let albumKey09 = AlbumKey(mbid: nil, name: "Tattoo", artist: "Rory Gallagher")
            
            let service00 = NetworkServiceType.albumInfo(albumKey: albumKey00) { albumInfo in }
            let service01 = NetworkServiceType.albumInfo(albumKey: albumKey01) { albumInfo in }
            let service02 = NetworkServiceType.albumInfo(albumKey: albumKey02) { albumInfo in }
            let service03 = NetworkServiceType.albumInfo(albumKey: albumKey03) { albumInfo in }
            let service04 = NetworkServiceType.albumInfo(albumKey: albumKey04) { albumInfo in }
            let service05 = NetworkServiceType.albumInfo(albumKey: albumKey05) { albumInfo in }
            let service06 = NetworkServiceType.albumInfo(albumKey: albumKey06) { albumInfo in }
            let service07 = NetworkServiceType.albumInfo(albumKey: albumKey07) { albumInfo in }
            let service08 = NetworkServiceType.albumInfo(albumKey: albumKey08) { albumInfo in }
            let service09 = NetworkServiceType.albumInfo(albumKey: albumKey09) { albumInfo in }

            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service01)
        }
        
        
        if true {
            let artistKey = ArtistKey(mbid: "1801bd47-46ae-49ff-bfcd-6e01b562d880", name: nil)
            let service00 = NetworkServiceType.albumList(artistKey: artistKey, page: 1) { albumList in }
            let service01 = NetworkServiceType.albumList(artistKey: artistKey, page: 2) { albumList in }
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service01)
            let _ = NetworkService.shared.request(service00)
            let _ = NetworkService.shared.request(service01)
        }

        waitForExpectations(timeout: 5) { error in
            print("==============> operationQueue.count: \(NetworkService.shared.operationQueue.operations.count), serviceHistory: \(NetworkService.shared.serviceHistory.count)")
            XCTAssert(false)
        }
        
    }
    
    func testCodable_encodeArtistKey() {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let artistKey = ArtistKey(mbid: "606bf117-494f-4864-891f-09d63ff6aa4b", name: "Rise Against")
        let expectResult = """
            {
              "name" : "Rise Against",
              "mbid" : "606bf117-494f-4864-891f-09d63ff6aa4b"
            }
            """

        do {
            let data: Data = try encoder.encode(artistKey)
            let encoded = String.init(data: data, encoding: .utf8)!
            print(encoded)
            
            if encoded == expectResult {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            XCTAssert(false)
        }
    }

    func testCodable_decodeArtistKey() {
        let decoder = JSONDecoder()
        let artistKey = ArtistKey(mbid: "606bf117-494f-4864-891f-09d63ff6aa4b", name: "Rise Against")
        let json = """
            {
              "name" : "Rise Against",
              "mbid" : "606bf117-494f-4864-891f-09d63ff6aa4b"
            }
            """
        guard let data = json.data(using: .utf8) else {
            XCTAssert(false)
            return
        }
        
        do {
            let decoded = try decoder.decode(ArtistKey.self, from: data)
            print(decoded)
            
            if decoded == artistKey {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            XCTAssert(false)
        }
    }

    // requestAlbumList(artistKey: ArtistKey)
    
    func testCodable_encodeService_requestAlbumList() {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        let artistKey = ArtistKey(mbid: "606bf117-494f-4864-891f-09d63ff6aa4b", name: "Rise Against")
        let service = ServiceType.albumList(artistKey: artistKey)
        let expectResult = """
            {
              "albumList" : {
                "name" : "Rise Against",
                "mbid" : "606bf117-494f-4864-891f-09d63ff6aa4b"
              }
            }
            """
        
        do {
            let data: Data = try encoder.encode(service)
            let encoded = String.init(data: data, encoding: .utf8)!
            print(encoded)
            
            if encoded == expectResult {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            XCTAssert(false)
        }
    }
    
    func testCodable_decodeService_requestAlbumList() {
        let decoder = JSONDecoder()
        let artistKey = ArtistKey(mbid: "606bf117-494f-4864-891f-09d63ff6aa4b", name: "Rise Against")
        let service = ServiceType.albumList(artistKey: artistKey)
        let json = """
            {
                "albumList" : {
                    "name" : "Rise Against",
                    "mbid" : "606bf117-494f-4864-891f-09d63ff6aa4b"
                }
            }
            """
        guard let data = json.data(using: .utf8) else {
            XCTAssert(false)
            return
        }
        
        do {
            let decoded = try decoder.decode(ServiceType.self, from: data)
            print(decoded)
            
            if decoded == service {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            XCTAssert(false)
        }
    }
        

    

    
}
