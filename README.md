# README #

### What is this repository for? ###
* This is a Swift sample project for my studying and at the same time for a demo.

### User senario ###
1. User search artists with a keyword.
1. The user selects a artist in the search results.
1. Display the albums of the artist.
1. When the user touches saves/delete button on each cell, it saves or deletes the album info to local.
1. When the user touches a album cell, it displays the album's detail info.
1. In the detail view, the user can save or delete the detail info.
1. When returning to the main view, it displays the saved albums.
1. When the user selects one of the albums, it displays the album's detail info view.
1. In the info view, the user can move to prev/next album with a swipe gesture.

### Framework, Tool ###
* Alamofire(JSON)
* CoreData
* Cocoapods

### LastFM API ###
* This project communicate with LastFM Service. (https://www.last.fm/api)
* I recommend you to creat your account, but you can run the project with my account.

### Design patterns ###
* I tried to use several design patterns.

* MVC-N
	+ ServiceManager
	+ NetworkService
	+ CacheService
	+ LocalService

* MVVM
	+ AlbumCollectionViewCell.ViewModel
	+ ArtistSearchTableViewCell.ViewModel
	+ AlbumListTableViewCell.ViewModel
	+ AlbumTrackTableViewCell.ViewModel
	+ AlbumDetailView.ViewModel

* MVP
	+ AlbumListCellPresenter

* Memento(Archiving)
	+ ServiceManager.saveHistory()
	+ ServiceManager.loadHistory()

### How to work the service classes? ###
1. NetworkService class is for JSON network which uses the Alamofire framework. 
1. CacheService Class save all the data from LastFM service into CoreData.
1. It always uses NSFetchedResultsController to display the data. 
1. When the user select save/delete the album, the LocalService Class save/delete the album info into/from CoreData.
1. All above procedures are operated by ServiceManager class.

### I got help from the posts below ###
* MVC-N pattern
	+ https://academy.realm.io/posts/slug-marcus-zarra-exploring-mvcn-swift/
	
* MVVM pattern
	+ http://matteomanferdini.com/mvvm-pattern-ios-swift/
	
* MVP pattern
	+ https://blog.moove-it.com/going-from-mvc-to-mvp-on-ios/

* Singleton, Facade, Decorator, Delegation, Adapter, Observer, Memento(Archiving) pattern
	+ https://www.raywenderlich.com/160651/design-patterns-ios-using-swift-part-12
	+ https://www.raywenderlich.com/160653/design-patterns-ios-using-swift-part-22
	
* Alamofire 
	+ https://medium.com/@shenghuawu/simple-ios-api-client-with-alamofire-cfb2cadf6c11






