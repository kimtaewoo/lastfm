//
//  LastFmUITests.swift
//  LastFmUITests
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import XCTest

class LastFmUITests: XCTestCase {
    var application : XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
        
        application = XCUIApplication()
        application.launchArguments.append("UI Testing")
        
        let parameters = ["userId" : "plokmijn",
                          "name" : "shane wu",
                          "email" : "shane.wu@gmail.com",
                          "description" : "I'm shane wu"]
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString = String(data: data, encoding: String.Encoding.utf8)
        application.launchEnvironment["http://localhost:3000/user/plokmijn"] = jsonString
        application.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    
    func testExample() {
        application.launch()
        
        // Make sure we're displaying onboarding
        XCTAssertTrue(application.isDisplayingMainView)

//        // Swipe left three times to go through the pages
//        app.swipeLeft()
//        app.swipeLeft()
//        app.swipeLeft()
        
        // Tap the "Done" button
        application.buttons["Search"].tap()
        
        // Onboarding should no longer be displayed
        XCTAssertTrue(application.isDisplayingArtistList)
    }
    
}
