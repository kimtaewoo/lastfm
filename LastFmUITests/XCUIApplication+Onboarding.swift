//
//  XCUIApplication+Onboarding.swift
//  LastFm
//
//  Created by ktw on 30.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import XCTest

extension XCUIApplication {
    var isDisplayingMainView: Bool {
        return collectionViews["Main"].exists
    }
    
    var isDisplayingArtistList: Bool {
        return tables["Artist"].exists
    }
    
    var isDisplayingAlbumList: Bool {
        return tables["Album"].exists
    }
    
    var isDisplayingAlbumDetail: Bool {
        return tables["AlbumDetail"].exists
    }
}
