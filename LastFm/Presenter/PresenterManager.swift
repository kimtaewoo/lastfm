//
//  PresenterManager.swift
//  LastFm
//
//  Created by ktw on 06.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

class PresenterManager {
    private var presenterContainer: [AnyHashable : AnyObject] = [:]
    
    func makePresenter(ofKey key: AnyHashable, presenter: AnyObject) -> AnyObject? {
        if nil == presenterContainer[key] {
            presenterContainer[key] = presenter
        }
        return presenterContainer[key]
    }
    
    func removeAllPresenters() {
        presenterContainer.removeAll()
    }
}
