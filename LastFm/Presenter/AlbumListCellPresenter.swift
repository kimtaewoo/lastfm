//
//  AlbumListCellPresenter.swift
//  LastFm
//
//  Created by ktw on 06.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol AlbumListCellPresenterProtocol {
    func saveAlbumInfo()
    func deleteAlbumInfo()
}

class AlbumListCellPresenter: NSObject {
    weak var tableView: UITableView?
    var indexPath: IndexPath?
    var albumKey: AlbumKey?
    var fetchedResultsControllr: NSFetchedResultsController<NSFetchRequestResult>?
    
    init(_ tableView: UITableView?, indexPath: IndexPath?, albumKey: AlbumKey?) {
        self.tableView = tableView
        self.indexPath = indexPath
        self.albumKey = albumKey
    }
}

extension AlbumListCellPresenter: AlbumListCellPresenterProtocol {
    
    func saveAlbumInfo() {
        guard let albumKey = albumKey else {
            return
        }
        
        if nil == fetchedResultsControllr {
            fetchedResultsControllr = ServiceManager.shared.queryAlbumInfo(albumKey.localKey)
            fetchedResultsControllr?.delegate = self
        }
        ServiceManager.shared.fetchAlbumInfo(albumKey)
    }
    
    func deleteAlbumInfo() {
        guard let albumKey = albumKey else {
            return
        }
        
        if nil == fetchedResultsControllr {
            fetchedResultsControllr = ServiceManager.shared.queryAlbumInfo(albumKey.localKey)
            fetchedResultsControllr?.delegate = self
        }
        ServiceManager.shared.deleteAlbumInfo(albumKey.localKey)
    }
}


extension AlbumListCellPresenter: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let tableView = tableView,
            let indexPath = indexPath else {
            return
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}

