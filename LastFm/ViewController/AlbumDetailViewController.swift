//
//  AlbumDetailViewController.swift
//  LastFm
//
//  Created by ktw on 21.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import CoreData

protocol AlbumDetailViewControllerDataSource : class {
    func fetchCurrentAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>?
    func fetchPrevAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>?
    func fetchNextAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>?
}

class AlbumDetailViewController: UIViewController {
    @IBOutlet weak var viewAlbumDetail: AlbumDetailView!
    @IBOutlet weak var tableViewTracks: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelNoTracks: UILabel!
    @IBOutlet weak var swipeNext: UISwipeGestureRecognizer!
    @IBOutlet weak var swipePrev: UISwipeGestureRecognizer!

    var btnSave : UIBarButtonItem?
    var btnDelete : UIBarButtonItem?
    var btnDone : UIBarButtonItem?
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    weak var delegate : AlbumDetailViewControllerDataSource?
    var currentAlbumInfo : AlbumInfoProtocol? {
            get {
                return fetchedResultsController?.fetchedObjects?.first as? AlbumInfoProtocol
            }
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableViewTracks.accessibilityIdentifier = "AlbumDetail"
        activityIndicator.stopAnimating();
        
        tableViewTracks.rowHeight = UITableViewAutomaticDimension
        tableViewTracks.estimatedRowHeight = 60
        tableViewTracks.dataSource = self
        view.addGestureRecognizer(swipeNext!)
        view.addGestureRecognizer(swipePrev!)
        
        viewAlbumDetail.delegate = self
        
        btnSave = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(onTouch_save(_:)))
        btnDelete = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(onTouch_delete(_:)))
        btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onTouch_done(_:)))
        
        fetchedResultsController = delegate?.fetchCurrentAlbumInfo()
        fetchedResultsController?.delegate = self
        displayAlbumInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func displayAlbumInfo() {
        if let info = currentAlbumInfo {
            viewAlbumDetail.viewModel = AlbumDetailView.ViewModel(info)
        }
        else {
            viewAlbumDetail.viewModel = AlbumDetailView.ViewModel()
            
        }
        tableViewTracks.reloadData()
        setNavigationBarButton()
    }

    func setNavigationBarButton() {
        if let info = currentAlbumInfo {
            if true == ServiceManager.shared.checkAlbumInfoSaved(info.localKey) {
                navigationItem.rightBarButtonItems = [btnDone!, btnDelete!]
            }
            else {
                navigationItem.rightBarButtonItems = [btnDone!, btnSave!]
            }
        }
        else {
            navigationItem.rightBarButtonItems = nil
        }
    }
}

//
// MARK: - swipe gesture
//
extension AlbumDetailViewController {

    @IBAction func onSwipePrev(_ sender: UISwipeGestureRecognizer) {
        let _ = moveToPrevAlbum()
    }

    @IBAction func onSwipeNext(_ sender: UISwipeGestureRecognizer) {
        let _ = moveToNextAlbum()
    }

    func refreshCurrentAlbum() -> Bool?{
        guard let delegate = delegate,
            let fetchedCtrl = delegate.fetchCurrentAlbumInfo() else {
                return false
        }
        fetchedResultsController = fetchedCtrl
        fetchedResultsController?.delegate = self
        displayAlbumInfo()
        slideEffectNext()
        return true
    }
    
    func moveToPrevAlbum() -> Bool?{
        guard let delegate = delegate,
            let fetchedCtrl = delegate.fetchPrevAlbumInfo() else {
                return false
        }
        fetchedResultsController = fetchedCtrl
        fetchedResultsController?.delegate = self
        displayAlbumInfo()
        slideEffectPrev()
        return true
    }

    func moveToNextAlbum() -> Bool? {
        guard let delegate = delegate,
            let fetchedCtrl = delegate.fetchNextAlbumInfo() else {
                return false
        }
        fetchedResultsController = fetchedCtrl
        fetchedResultsController?.delegate = self
        displayAlbumInfo()
        slideEffectNext()
        return true
    }

    func slideEffectPrev() {
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        viewAlbumDetail.layer.add(transition, forKey: nil)
    }

    func slideEffectNext() {
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        viewAlbumDetail.layer.add(transition, forKey: nil)
    }
}

//
// MARK: - Button
//
extension AlbumDetailViewController {

    func onTouch_save(_ sender: Any) {
        if let albumInfo = currentAlbumInfo {
            ServiceManager.shared.saveAlbumInfo(albumInfo)
        }
        setNavigationBarButton()
    }
    
    func onTouch_delete(_ sender: Any) {
        if let albumInfo = currentAlbumInfo {
            ServiceManager.shared.deleteAlbumInfo(albumInfo.localKey)
        }
        setNavigationBarButton()
    }

    func onTouch_done(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}

//
// MARK: - NSFetchedResultsControllerDelegate
//
extension AlbumDetailViewController : NSFetchedResultsControllerDelegate {

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            print("Insert Object: \(String(describing: newIndexPath))")
            displayAlbumInfo()
            break
        case .delete:
            print("Delete Object: \(String(describing: indexPath))")
            if false == refreshCurrentAlbum(),
                false == moveToPrevAlbum() {
                navigationController?.popViewController(animated: true)
            }
            break
        case .update:
            print("Update Object: \(String(describing: indexPath))")
            displayAlbumInfo()
            break
        case .move:
            print("Move Object: \(String(describing: indexPath))")
            break
        } // switch
    }
}


//
// MARK: - UITableViewDataSource
//
extension AlbumDetailViewController : UITableViewDataSource {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let albumInfo = currentAlbumInfo,
            let tracks = albumInfo.tracks else {
            labelNoTracks.isHidden = false
            return 0
        }
        
        labelNoTracks.isHidden = (0 < tracks.count)
        return tracks.count
    }


    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTrackTableViewCell", for: indexPath) as! AlbumTrackTableViewCell

        guard let albumInfo = currentAlbumInfo,
            let tracks = albumInfo.tracks,
            let track = tracks.allObjects[indexPath.row] as? TrackProtocol else {
            cell.viewModel = AlbumTrackTableViewCell.ViewModel()
            return cell
        }
        cell.viewModel = AlbumTrackTableViewCell.ViewModel(track)
        return cell
    }
}

//
// MARK: - AlbumDetailViewDelegate
//
extension AlbumDetailViewController : AlbumDetailViewDelegate {
    func moveToPrevAlbumDetail() {
        let _ = moveToPrevAlbum()
    }
    
    func moveToNextAlbumDetail() {
        let _ = moveToNextAlbum()
    }
}






