//
//  ArtistSearchViewController.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import CoreData


class ArtistSearchViewController: UIViewController {
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelNoArtists: UILabel!
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    var service : ServiceType!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewSearch.accessibilityIdentifier = "Artist"
        activityIndicator.stopAnimating();
        
        // ready refresh control
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ArtistSearchViewController.refresh), for: UIControlEvents.valueChanged)
        tableViewSearch.addSubview(refreshControl)
        
        textFieldSearch.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AlbumListSegue" {
            if let indexPath = tableViewSearch.indexPathForSelectedRow,
                let artist = fetchedResultsController?.object(at: indexPath) as? ArtistProtocol,
                let viewController = segue.destination as? AlbumListViewController {
                tableViewSearch.deselectRow(at: indexPath, animated: true)
                viewController.artistKey = ArtistKey(mbid: artist.mbid, name: artist.name)
            }
        }
    }

    @IBAction func onTouch_search(_ sender: Any) {
        textFieldSearch.resignFirstResponder()
        search()
    }
    
    func search() {
        guard let keyword = textFieldSearch?.text else {
            let alert = UIAlertController(title: "Alert", message: "Type search keyword.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        service = ServiceType.artistList(artistSearchKey: ArtistSearchKey(name: keyword))
        guard let fetchedCtrl = ServiceManager.shared.queryService(service) else {
            return
        }
        
        fetchedResultsController = fetchedCtrl
        fetchedResultsController?.delegate = self
        tableViewSearch.reloadData()
        
        if 0 == fetchedResultsController?.fetchedObjects?.count {
            ServiceManager.shared.requestService(service, completionHandler: nil)
        }
    }
    
    func refresh() {
        refreshControl.endRefreshing()
        if let service = service {
            ServiceManager.shared.emptyService(service)
            ServiceManager.shared.requestService(service, completionHandler: nil)
            tableViewSearch.reloadData()
        }
    }
    
    func nextPage() {
        ServiceManager.shared.requestService(service, completionHandler: nil)
    }
}


//
// MARK: - NSFetchedResultsControllerDelegate
//
extension ArtistSearchViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let tableView = tableViewSearch else {
            return
        }
        
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        guard let tableView = tableViewSearch else {
            return
        }
        
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        guard let tableView = tableViewSearch else {
            return
        }
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }


    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let tableView = tableViewSearch else {
            return
        }
        
        print("--------- controllerDidChangeContent")
        tableView.endUpdates()
    }
}

//
// MARK: - UITextFieldDelegate
//
extension ArtistSearchViewController : UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        search()
        return true;
    }
}

//
// MARK: - UITableViewDataSource
//
extension ArtistSearchViewController : UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            print("No sections in fetchedResultsController")
            labelNoArtists.isHidden = false
            return 0
        }
        let sectionInfo = sections[section]
        labelNoArtists.isHidden = (0 < sectionInfo.numberOfObjects)
        return sectionInfo.numberOfObjects
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistSearchCell", for: indexPath) as! ArtistSearchTableViewCell
        if let artist = fetchedResultsController?.object(at: indexPath) as? ArtistProtocol  {
            cell.viewModel = ArtistSearchTableViewCell.ViewModel(artist)
        }
        else {
            cell.viewModel = ArtistSearchTableViewCell.ViewModel()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rows = tableView.numberOfRows(inSection: indexPath.section)
        print("------------ willDisplay rows: \(rows), indexPath.row: \(indexPath.row)")

        if indexPath.row == rows - 1 {
            nextPage()
        }
    }
}

//
// MARK: - UITableViewDelegate
//
extension ArtistSearchViewController : UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = fetchedResultsController?.object(at: indexPath) as? ArtistProtocol {
            super.performSegue(withIdentifier: "AlbumListSegue", sender: self)
        }
    }
}









