//
//  AlbumListViewController.swift
//  LastFm
//
//  Created by ktw on 21.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData


class AlbumListViewController: UIViewController {

    @IBOutlet weak var tableViewAlbum: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelNoAlbums: UILabel!
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var artistKey : ArtistKey?
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    var service : ServiceType?
    var currentIndexPath : IndexPath?
    var savedInfo : [IndexPath : Bool] = [:]
    var presenterManager = PresenterManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewAlbum.accessibilityIdentifier = "Album"
        activityIndicator.stopAnimating();
        
        // ready refresh control
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(AlbumListViewController.refresh), for: UIControlEvents.valueChanged)
        tableViewAlbum.addSubview(refreshControl)
        
        search()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        savedInfo.removeAll(keepingCapacity: false)
        tableViewAlbum.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AlbumDetailSegue" {
            if let indexPath = tableViewAlbum.indexPathForSelectedRow,
                let viewController = segue.destination as? AlbumDetailViewController {
                currentIndexPath = indexPath
                tableViewAlbum.deselectRow(at: indexPath, animated: true)
                viewController.delegate = self
            }
        }
    }
    
    func search() {
        guard let artistKey = artistKey else {
            return
        }
        
        presenterManager.removeAllPresenters()
        service = ServiceType.albumList(artistKey: artistKey)
        guard let fetchedCtrl = ServiceManager.shared.queryService(service) else {
            return
        }
        
        fetchedResultsController = fetchedCtrl
        fetchedResultsController?.delegate = self
        tableViewAlbum.reloadData()
        
        if 0 == fetchedResultsController?.fetchedObjects?.count {
            ServiceManager.shared.requestService(service, completionHandler: nil)
        }
    }
    
    
    func refresh() {
        refreshControl.endRefreshing()
        
        if let service = service {
            ServiceManager.shared.emptyService(service)
            ServiceManager.shared.requestService(service, completionHandler: nil)
            tableViewAlbum.reloadData()
        }
    }
    
    
    func nextPage() {
        ServiceManager.shared.requestService(service, completionHandler: nil)
    }

}


//
// MARK: - NSFetchedResultsControllerDelegate
//
extension AlbumListViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let tableView = tableViewAlbum else {
            return
        }
        
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        guard let tableView = tableViewAlbum else {
            return
        }
        
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        guard let tableView = tableViewAlbum else {
            return
        }
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let tableView = tableViewAlbum else {
            return
        }
        
        tableView.endUpdates()
    }
}


//
// MARK: - UITableViewDataSource
//
extension AlbumListViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            print("No sections in fetchedResultsController")
            return 0
        }
        
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            labelNoAlbums.isHidden = false
            return 0
        }
        let sectionInfo = sections[section]
        labelNoAlbums.isHidden = (0 < sectionInfo.numberOfObjects)
        return sectionInfo.numberOfObjects
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumListTableViewCell", for: indexPath) as! AlbumListTableViewCell
       
        if let album = fetchedResultsController?.object(at: indexPath) as? AlbumProtocol {
            let albumKey = AlbumKey(mbid: album.mbid, name: album.name, artist: album.artistName)
            let saved = ServiceManager.shared.checkAlbumInfoSaved(albumKey.localKey)
            
            print("cell for rowatindex=\(indexPath.description), saved=\(saved)")
            
            cell.viewModel = AlbumListTableViewCell.ViewModel(album, saved: saved)
            cell.saveAction = { [weak self] in
                if let presenter = self?.presenterManager.makePresenter(ofKey: indexPath, presenter: AlbumListCellPresenter(tableView, indexPath: indexPath, albumKey: albumKey)) as? AlbumListCellPresenterProtocol {
                    presenter.saveAlbumInfo()
                }
            }
            cell.deleteAction = { [weak self] in
                if let presenter = self?.presenterManager.makePresenter(ofKey: indexPath, presenter: AlbumListCellPresenter(tableView, indexPath: indexPath, albumKey: albumKey)) as? AlbumListCellPresenterProtocol {
                    presenter.deleteAlbumInfo()
                }
            }
        }
        else {
            cell.viewModel = AlbumListTableViewCell.ViewModel()
        }
         
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rows = tableView.numberOfRows(inSection: indexPath.section)
        print("------------ willDisplay rows: \(rows), indexPath.row: \(indexPath.row)")

        if indexPath.row == rows - 1 {
            nextPage()
            print("------------ >>>> loadnextpage")
        }
    }
}

//
// MARK: - UITableViewDelegate
//
extension AlbumListViewController : UITableViewDelegate {
    
   public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.performSegue(withIdentifier: "AlbumDetailSegue", sender: self)
    }
    
}

//
// MARK: - AlbumDetailViewControllerDataSource
//
extension AlbumListViewController : AlbumDetailViewControllerDataSource {
    
    func fetchCurrentAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let indexPath = currentIndexPath,
            let album = fetchedResultsController?.object(at: indexPath) as? AlbumProtocol else {
                return nil
        }

        let albumKey = AlbumKey(mbid: album.mbid, name: album.name, artist: album.artistName)
        let service = ServiceType.albumInfo(albumKey: albumKey)
        guard let albumInfoFetchedCtrl = ServiceManager.shared.queryService(service) else {
            return nil
        }

        if 0 == albumInfoFetchedCtrl.fetchedObjects?.count {
            ServiceManager.shared.requestService(service, completionHandler: nil)
        }
        
        return albumInfoFetchedCtrl
    }
    
    func fetchPrevAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let indexPath = currentIndexPath?.prev() else {
            return nil
        }
        currentIndexPath = indexPath
        return fetchCurrentAlbumInfo()
    }
    
    func fetchNextAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let count = fetchedResultsController?.fetchedObjects?.count,
            let indexPath = currentIndexPath?.next(lessThan: count) else {
            return nil
        }
        currentIndexPath = indexPath
        return fetchCurrentAlbumInfo()
    }
}




