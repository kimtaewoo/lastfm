//
//  ViewController.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import CoreData

class CollectionViewController: UIViewController {
    
    @IBOutlet weak var collectionViewAlbums: UICollectionView!
    @IBOutlet weak var labelNoAlbums: UILabel!
    
    var blockOperations: [BlockOperation] = []
    var currentIndexPath : IndexPath?
    lazy var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>? = { return ServiceManager.shared.queryAlbumCollection() }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        collectionViewAlbums.accessibilityIdentifier = "Main"
        
        let btnSearch = UIBarButtonItem(image: UIImage(named: "signia-99"), style: .plain, target: self, action: #selector(onTouch_search(_:)))
        btnSearch.accessibilityIdentifier = "Search"
        navigationItem.rightBarButtonItems = [btnSearch]

        fetchedResultsController?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        // Cancel all block operations when VC deallocates
        for operation: BlockOperation in blockOperations {
            operation.cancel()
        }
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func onTouch_search(_ sender: Any) {
        performSegue(withIdentifier: "ArtistSearchSegue", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AlbumDetailFromMainSegue" {
            if let indexPath = collectionViewAlbums.indexPathsForSelectedItems?.first,
                let viewController = segue.destination as? AlbumDetailViewController {
                currentIndexPath = indexPath
                collectionViewAlbums.deselectItem(at: indexPath, animated: true)
                viewController.delegate = self
            }
        }
    }
}

//
// MARK: - NSFetchedResultsControllerDelegate
//
extension CollectionViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // collectionViewAlbums.beginUpdates()
        blockOperations.removeAll(keepingCapacity: false)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            print("Insert Section: \(sectionIndex)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.insertSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
            break
            
        case .delete:
            print("Delete Section: \(sectionIndex)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
            break
            
        case .move:
            break
            
        case .update:
            print("Update Section: \(sectionIndex)")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.reloadSections(NSIndexSet(index: sectionIndex) as IndexSet)
                    }
                })
            )
            break
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            print("Insert Object: \(String(describing: newIndexPath))")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.insertItems(at: [newIndexPath!])
                    }
                })
            )
            break
            
        case .delete:
            print("Delete Object: \(String(describing: indexPath))")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.deleteItems(at: [indexPath!])
                    }
                })
            )
            break
            
        case .update:
            print("Update Object: \(String(describing: indexPath))")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.reloadItems(at: [indexPath!])
                    }
                })
            )
            break
            
        case .move:
            print("Move Object: \(String(describing: indexPath))")
            blockOperations.append(
                BlockOperation(block: { [weak self] in
                    if let this = self {
                        this.collectionViewAlbums!.moveItem(at: indexPath!, to: newIndexPath!)
                    }
                })
            )
            break
            
        } // switch
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // collectionViewAlbums.endUpdates()
        collectionViewAlbums!.performBatchUpdates({ () -> Void in
            for operation: BlockOperation in self.blockOperations {
                operation.start()
            }
        }, completion: { (finished) -> Void in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
}

//
// MARK: - UICollectionViewDataSource
//
extension CollectionViewController: UICollectionViewDataSource {

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let fetchedObjects = fetchedResultsController?.fetchedObjects else {
            return 0
        }
        labelNoAlbums.isHidden = (0 < fetchedObjects.count)
        return fetchedObjects.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionViewCell", for: indexPath) as! AlbumCollectionViewCell

        if let album = fetchedResultsController?.object(at: indexPath) as? AlbumInfoProtocol {
            cell.viewModel = AlbumCollectionViewCell.ViewModel(album)
        }
        else {
            cell.viewModel = AlbumCollectionViewCell.ViewModel()
        }
        
        return cell
    }
}

//
// MARK: - UICollectionViewDelegate
//
extension CollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        super.performSegue(withIdentifier: "AlbumDetailFromMainSegue", sender: self)
    }
}


//
// MARK: - UICollectionViewDelegateFlowLayout
//
extension CollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (view.frame.size.width / 3.0) - 2
        return CGSize(width: w, height: w)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
}

//
// MARK: - AlbumDetailViewControllerDataSource
//
extension CollectionViewController : AlbumDetailViewControllerDataSource {
    
    func fetchCurrentAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let indexPath = currentIndexPath,
            let fetchedObjects = fetchedResultsController?.fetchedObjects,
            indexPath.row < fetchedObjects.count,
            let albumInfo = fetchedResultsController?.object(at: indexPath) as? AlbumInfoProtocol else {
                return nil
        }
        return ServiceManager.shared.queryAlbumInfo(albumInfo.localKey)
    }
    
    func fetchPrevAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let indexPath = currentIndexPath?.prev() else {
            return nil
        }
        currentIndexPath = indexPath
        return fetchCurrentAlbumInfo()
    }
    
    func fetchNextAlbumInfo() -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let count = fetchedResultsController?.fetchedObjects?.count,
            let indexPath = currentIndexPath?.next(lessThan: count) else {
                return nil
        }
        currentIndexPath = indexPath
        return fetchCurrentAlbumInfo()
    }
}







