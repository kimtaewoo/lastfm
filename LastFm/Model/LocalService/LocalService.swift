//
//  LocalService.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class LocalService {
    static let shared = LocalService()
    
    private init() {

    }
    
    static var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        
        let modelURL = Bundle(for: LocalService.self).url(forResource: "Local", withExtension: "momd")! // type your database name here..
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let documentDirectory: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let url = documentDirectory?.appendingPathComponent("Local.sqlite") // type your database name here...
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    static var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    static func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

//
// MARK: - LocalServiceProtocol
//
extension LocalService : LocalServiceProtocol {
    func saveAlbumInfo(_ albumInfo: AlbumInfoProtocol?) {
        guard let albumInfo = albumInfo else {
            return
        }
        
        deleteAlbumInfo(albumInfo.localKey)
        
        let album = NSEntityDescription.insertNewObject(forEntityName: "AlbumInfo", into: LocalService.managedObjectContext) as! AlbumInfo
        album.localKey = albumInfo.localKey
        album.mbid = albumInfo.mbid
        album.name = albumInfo.name
        album.artistName = albumInfo.artistName
        album.imageUrl = albumInfo.imageUrl
        album.savedTime = NSDate()
        
        if let tracks = albumInfo.tracks {
            for case let track as TrackProtocol in tracks {
                let dbTrack = NSEntityDescription.insertNewObject(forEntityName: "Track", into: LocalService.managedObjectContext) as! Track
                dbTrack.name = track.name
                dbTrack.url = track.url
                dbTrack.duration = track.duration
                dbTrack.album = album
            }
        }
        LocalService.saveContext()
    }
    
    
    func deleteAlbumInfo(_ localKey: String?) {
        guard let localKey = localKey else {
            return
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
        fetchRequest.predicate = NSPredicate(format: "localKey == %@", localKey)
        
        // let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        // do {
        //     try LocalService.managedObjectContext.execute(deleteRequest)
        // } catch let error as NSError {
        //     print(error.localizedDescription)
        // }
        
        do {
            if let results = try LocalService.managedObjectContext.fetch(fetchRequest) as? [AlbumInfo] {
                for albumInfo in results {
                    LocalService.managedObjectContext.delete(albumInfo)
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        LocalService.saveContext()
    }
    
    
    func queryAlbumInfo(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let localKey = localKey else {
            return nil
        }
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
        fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "savedTime", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: LocalService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        // Configure Fetched Results Controller
        return fetchedResultsController
    }
    
    
    func queryAlbumCollection() -> NSFetchedResultsController<NSFetchRequestResult>? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
        // fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "savedTime", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: LocalService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        // Configure Fetched Results Controller
        return fetchedResultsController
    }


    func checkAlbumInfoSaved(_ localKey: String?) -> Bool {
        guard let localKey = localKey else {
            return false
        }
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
        fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        do {
            let cnt = try LocalService.managedObjectContext.count(for: fetchRequest)
            return (0 < cnt)
        } catch let error as NSError {
            print(error.localizedDescription)
            return false
        }
    }


}

