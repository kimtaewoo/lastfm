//
//  Track+CoreDataClass.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Track)
public class Track: NSManagedObject {

}

extension Track: TrackProtocol {
    var albumInfo : AlbumInfoProtocol? {
        get {
            return album
        }
    }
}
