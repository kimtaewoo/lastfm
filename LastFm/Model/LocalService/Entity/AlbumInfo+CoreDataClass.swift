//
//  AlbumInfo+CoreDataClass.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData

@objc(AlbumInfo)
public class AlbumInfo: NSManagedObject {

}

extension AlbumInfo: AlbumInfoProtocol {

}
