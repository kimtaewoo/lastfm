//
//  Track+CoreDataProperties.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData


extension Track {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Track> {
        return NSFetchRequest<Track>(entityName: "Track")
    }

    @NSManaged public var duration: String?
    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var album: AlbumInfo?

}
