//
//  AlbumInfo+CoreDataProperties.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData


extension AlbumInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AlbumInfo> {
        return NSFetchRequest<AlbumInfo>(entityName: "AlbumInfo")
    }

    @NSManaged public var artistName: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var localKey: String?
    @NSManaged public var mbid: String?
    @NSManaged public var name: String?
    @NSManaged public var savedTime: NSDate?
    @NSManaged public var tracks: NSSet?

}

// MARK: Generated accessors for tracks
extension AlbumInfo {

    @objc(addTracksObject:)
    @NSManaged public func addToTracks(_ value: Track)

    @objc(removeTracksObject:)
    @NSManaged public func removeFromTracks(_ value: Track)

    @objc(addTracks:)
    @NSManaged public func addToTracks(_ values: NSSet)

    @objc(removeTracks:)
    @NSManaged public func removeFromTracks(_ values: NSSet)

}
