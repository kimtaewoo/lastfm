//
//  NetworkService.swift
//  LastFm
//
//  Created by ktw on 19.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


class NetworkService {
    static let shared = NetworkService()
    var serviceHistory = [NetworkServiceType : Operation]()
    lazy var operationQueue : OperationQueue = {
        var oq = OperationQueue()
        oq.name = "NetworkService queue"
        //oq.maxConcurrentOperationCount = 1
        return oq
    }()
    
    private init() {
    }

}

//
// MARK: - NetworkServiceProtocol
//
extension NetworkService : NetworkServiceProtocol {
    func request(_ service: NetworkServiceType) -> Bool {
        if let _ = serviceHistory[service] {
            return false
        }
        
        let operation : Operation = service.operation
        operation.completionBlock = {
            self.serviceHistory.removeValue(forKey: service)
        }
        operationQueue.addOperation(operation)
        serviceHistory[service] = operation
        return true
    }
    
    static func errorCheck(_ response: DataResponse<Any>) -> Bool {
        guard response.result.isSuccess else {
            print("Error while fetching artists: \(String(describing: response.result.error))")
            return false
        }
        
        if let error = Mapper<LFError>().map(JSONObject: response.value),
            let code = error.error {
            print("Error while fetching artists: \(String(describing: code)), \(String(describing: error.message))")
            return false
        }
        
        return true
    }
}


//
// MARK: - var operation:
//
extension NetworkServiceType {
    
    var operation: BlockOperation {
        switch self {
        case .artistList(let artistSearchKey, let page, let completionHandler):
            let operation = BlockOperation {
                let sema = DispatchSemaphore(value: 0);
                print("--------- requestArtistList page: \(page)")
                Alamofire.request(LastFmApi.getArtistList(keyword: artistSearchKey.name, page: page)).responseJSON { response in
                    if NetworkService.errorCheck(response),
                        let artistResponse = Mapper<LFArtistResponse>().map(JSONObject: response.value),
                        let artists = artistResponse.artists {
                        completionHandler(artists)
                    }
                    sema.signal()
                }
                sema.wait()
            }
            return operation
            
        case .albumList(let artistKey, let page, let completionHandler):
            let operation = BlockOperation {
                let sema = DispatchSemaphore(value: 0);
                print("--------- requestAlbumList page: \(page)")
                Alamofire.request(LastFmApi.getArtistAlbums(mbid: artistKey.mbid, artistName: artistKey.name, page: page)).responseJSON { response in
                    if NetworkService.errorCheck(response),
                        let albumResponse = Mapper<LFAlbumResponse>().map(JSONObject: response.value),
                        let albums = albumResponse.albums {
                        completionHandler(albums)
                    }
                    sema.signal()
                }
                sema.wait()
            }
            return operation
            
        case .albumInfo(let albumKey, let completionHandler):
            let operation = BlockOperation {
                let sema = DispatchSemaphore(value: 0);
                print("--------- requestAlbumInfo:")
                Alamofire.request(LastFmApi.getAlbumInfo(mbid: albumKey.mbid, artistName: albumKey.artist, albumName: albumKey.name)).responseJSON { response in
                    if NetworkService.errorCheck(response),
                        let infoResponse = Mapper<LFAlbumInfoResponse>().map(JSONObject: response.value) {
                        completionHandler(infoResponse)
                    }
                    sema.signal()
                }
                sema.wait()
            }
            return operation
        } // switch
    } // var operation
    

}
