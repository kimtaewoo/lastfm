//
//  LFArtistResponse.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFArtistResponse: Mappable {
    var artists: [LFArtist]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        artists <- map["results.artistmatches.artist"]
    }
}
