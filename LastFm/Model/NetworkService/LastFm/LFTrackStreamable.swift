//
//  LFTrackStreamable.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFTrackStreamable: Mappable {
    var text : String?
    var fulltrack : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        text <- map["#text"]
        fulltrack <- map["fulltrack"]
    }
}
