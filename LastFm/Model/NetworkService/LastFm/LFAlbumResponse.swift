//
//  LFAlbumResponse.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFAlbumResponse: Mappable {
    var albums: [LFTopAlbum]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        albums <- map["topalbums.album"]
    }
}
