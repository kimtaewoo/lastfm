//
//  LFTopAlbum.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFTopAlbum: Mappable {
    var name : String?
    var playcount : Int64?
    var mbid : String?
    var url : String?
    var artist: LFAlbumArtist?
    var images: [LFAlbumImage]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        playcount <- map["playcount"]
        mbid <- map["mbid"]
        url <- map["url"]
        artist <- map["artist"]
        images <- map["image"]
    }
}
