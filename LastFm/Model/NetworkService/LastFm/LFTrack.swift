//
//  LFTrack.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFTrack: Mappable {
    var name : String?
    var url : String?
    var duration : String?
    var attr : LFTrackAttr?
    var streamable : LFTrackStreamable?
    var artist : LFTrackArtist?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        url <- map["url"]
        duration <- map["duration"]
        attr <- map["@attr"]
        streamable <- map["streamable"]
        artist <- map["artist"]
    }
}
