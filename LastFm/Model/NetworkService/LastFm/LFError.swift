//
//  LFError.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFError: Mappable {
    var error : Int?
    var message : String?
    var links : [String]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        links <- map["links"]
    }
}
