//
//  LFAlbumInfoResponse.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFAlbumInfoResponse: Mappable {
    var name : String?
    var artist : String?
    var mbid : String?
    var url : String?
    var listeners : Int64?
    var playcount : Int64?
    var images: [LFAlbumImage]?
    var tracks: LFTracks?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["album.name"]
        artist <- map["album.artist"]
        mbid <- map["album.mbid"]
        url <- map["album.url"]
        images <- map["album.image"]
        tracks <- map["album.tracks"]
    }
}
