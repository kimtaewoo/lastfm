//
//  LastFmApi.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import Alamofire

enum LastFmApi: URLRequestConvertible {
    static let baseURLPath = "http://ws.audioscrobbler.com/2.0/"
    static let authenticationToken = "b6fed80a64aa95beae4a3f824ca74e0c"

    case getArtistList(keyword: String?, page: Int)
    case getArtistAlbums(mbid: String?, artistName: String?, page: Int)
    case getAlbumInfo(mbid: String?, artistName: String?, albumName: String?)
}

extension LastFmApi {
    var httpMethod: HTTPMethod {
        switch self {
        case .getArtistList:    return .post
        case .getArtistAlbums:  return .post
        case .getAlbumInfo:     return .post
        }
    }
    
    var method: String {
        switch self {
        case .getArtistList:    return "artist.search"
        case .getArtistAlbums:  return "artist.gettopalbums"
        case .getAlbumInfo:     return "album.getinfo"
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let parameters: [String: Any] = {
            let limit = "30"
            var param = ["method" : method,
                         "api_key": LastFmApi.authenticationToken,
                         "limit" : limit,
                         "format": "json"]
            
            switch self {
            case .getArtistList(let keyword, let page):
                param += ["artist": (keyword ?? ""),
                          "page": String(page)]
                
            case .getArtistAlbums(let mbid, let artistName, let page):
                param += ["page": String(page)]
                if let mbid = mbid {
                    param += ["mbid" : mbid]
                }
                else {
                    param += ["artist": (artistName ?? "")]
                }
                
            case .getAlbumInfo(let mbid, let letartistName, let albumName):
                if let mbid = mbid {
                    param += ["mbid" : mbid]
                }
                else {
                    param += ["artist" : (letartistName ?? ""),
                              "album" : (albumName ?? "")]
                }
            }

            print("<< param: \(param)")
            
            return param
        }()
        
        let url = try LastFmApi.baseURLPath.asURL()        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
//        request.setValue(ImaggaRouter.authenticationToken, forHTTPHeaderField: "Authorization")
//        request.timeoutInterval = TimeInterval(10 * 1000)
        
        return try URLEncoding.default.encode(request, with: parameters)
    }
}


func += <K, V> (left: inout [K : V], right: [K : V]) {
    right.forEach{left[$0] = $1}
}


