//
//  LFAlbumArtist.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFAlbumArtist: Mappable {
    var mbid : String?
    var name : String?
    var url : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        mbid <- map["mbid"]
        name <- map["name"]
        url <- map["url"]
    }
}
