//
//  LFArtistImage.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper


class LFArtistImage: Mappable {
    
    var url : String?
    var size : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        url <- map["#text"]
        size <- map["size"]
    }
}
