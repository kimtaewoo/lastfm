//
//  LFArtist.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFArtist: Mappable {
    var listeners : Int64?
    var mbid : String?
    var name : String?
    var streamable : Bool?
    var url : String?
    var images: [LFArtistImage]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        listeners <- map["listeners"]
        mbid <- map["mbid"]
        name <- map["name"]
        streamable <- map["streamable"]
        url <- map["url"]
        images <- map["image"]
    }
}
