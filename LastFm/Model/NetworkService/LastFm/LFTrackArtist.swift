//
//  LFTrackArtist.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class LFTrackArtist: Mappable {
    var name : String?
    var mbid : String?
    var url : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        mbid <- map["mbid"]
        url <- map["url"]
    }
}
