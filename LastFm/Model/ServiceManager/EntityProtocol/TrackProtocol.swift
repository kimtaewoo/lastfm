//
//  TrackProtocol.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

protocol TrackProtocol {
    var duration: String? {get}
    var name: String? {get}
    var url: String? {get}
    var albumInfo: AlbumInfoProtocol? {get}
}

