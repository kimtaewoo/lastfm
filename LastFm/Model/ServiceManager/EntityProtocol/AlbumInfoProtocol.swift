//
//  AlbumInfoProtocol.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

protocol AlbumInfoProtocol {
    var artistName: String? {get}
    var imageUrl: String? {get}
    var localKey: String? {get}
    var mbid: String? {get}
    var name: String? {get}
    var tracks: NSSet? {get}
}
