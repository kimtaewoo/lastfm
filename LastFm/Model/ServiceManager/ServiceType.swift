//
//  ServiceType.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

enum ServiceType {
    case artistList(artistSearchKey: ArtistSearchKey)
    case albumList(artistKey: ArtistKey)
    case albumInfo(albumKey: AlbumKey)
}


//
// MARK: - Hashable
//
extension ServiceType : Hashable {
    var hashValue: Int  {
        switch self {
        case .artistList(let artistSearchKey): return artistSearchKey.hashValue
        case .albumList(let artistKey):        return artistKey.hashValue
        case .albumInfo(let albumKey):         return albumKey.hashValue
        }
    }
    
    static func == (lhs: ServiceType, rhs: ServiceType) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

//
// MARK: - Codable
//
extension ServiceType : Codable{
    enum CodingKeys: String, CodingKey {
        case artistList
        case albumList
        case albumInfo
    }
    
    enum CodingError: Error {
        case decodingError(String)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            if let codes = try? container.decode(ArtistSearchKey.self, forKey: .artistList) {
                self = .artistList(artistSearchKey: codes)
            }
            else if let codes = try? container.decode(ArtistKey.self, forKey: .albumList) {
                self = .albumList(artistKey: codes)
            }
            else if let codes = try? container.decode(AlbumKey.self, forKey: .albumInfo) {
                self = .albumInfo(albumKey: codes)
            }
            else {
                throw CodingError.decodingError("Could not decode \(decoder).")
            }
        } catch let error as NSError {
            throw CodingError.decodingError("Could not decode: \(error.localizedDescription).")
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            switch self {
            case .artistList(let artistSearchKey):
                try container.encode(artistSearchKey, forKey: .artistList)
            case .albumList(let artistKey):
                try container.encode(artistKey, forKey: .albumList)
            case .albumInfo(let albumKey):
                try container.encode(albumKey, forKey: .albumInfo)
            }
        } catch {
            throw CodingError.decodingError("Could not encode \(self).")
        }
    }
}
