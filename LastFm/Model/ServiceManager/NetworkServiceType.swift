//
//  NetworkServiceType.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

enum NetworkServiceType {
    case artistList(artistSearchKey: ArtistSearchKey, page: Int, completionHandler : ([AnyObject])->Void)
    case albumList(artistKey: ArtistKey, page: Int, completionHandler : ([AnyObject])->Void)
    case albumInfo(albumKey: AlbumKey, completionHandler : (AnyObject)->Void)
}

//
// MARK: - Hashable
//
extension NetworkServiceType : Hashable {
    static func == (lhs: NetworkServiceType, rhs: NetworkServiceType) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    var hashValue: Int  {
        switch self {
        case .artistList(let artistSearchKey, let page, _):  return artistSearchKey.hashValue ^ page.hashValue
        case .albumList(let artistKey, let page, _):         return artistKey.hashValue ^ page.hashValue
        case .albumInfo(let albumKey, _):                    return albumKey.hashValue
        }
    }
}
