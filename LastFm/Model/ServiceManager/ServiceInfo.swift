//
//  ServiceInfo.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import CoreData

class ServiceInfo : Codable {
    var page: Int
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    
    init(_ page: Int, fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?) {
        self.page = page
        self.fetchedResultsController = fetchedResultsController
    }
    
    enum CodingKeys: String, CodingKey {
        case page
    }
    
    enum CodingError: Error {
        case decodingError(String)
        case encodingError(String)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            page = try container.decode(Int.self, forKey: .page)
            print("-------- page decode: \(page)")
        } catch let error as NSError {
            throw CodingError.decodingError("Could not decode: \(error.localizedDescription).")
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        do {
            try container.encode(page, forKey: .page)
            print("-------- page encode: \(page)")
        } catch {
            throw CodingError.encodingError("Could not encode \(self).")
        }
    }
}
