//
//  LocalServiceProtocol.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import CoreData

protocol CacheServiceProtocol {
    func saveArtistList(_ localKey: String?, index: Int?, artists: [AnyObject]?)
    func saveAlbumList(_ localKey: String?, index: Int?, albums: [AnyObject]?)
    func saveAlbumInfo(_ localKey: String?, albumInfo: AnyObject?)
    func queryArtistList(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>?
    func queryAlbumList(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>?
    func queryAlbumInfo(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>?
    func empty(_ fetchedController: NSFetchedResultsController<NSFetchRequestResult>?)
}
