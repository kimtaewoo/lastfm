//
//  ServiceProtocol.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import CoreData

protocol ServiceProtocol {
    func queryService(_ service: ServiceType?) -> NSFetchedResultsController<NSFetchRequestResult>?
    func requestService(_ service: ServiceType?, completionHandler: ((_ fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>)->Void)?)
    func emptyService(_ service: ServiceType?)

    func fetchAlbumInfo(_ albumKey: AlbumKey?)
    func saveAlbumInfo(_ albumInfo: AlbumInfoProtocol?)
    func deleteAlbumInfo(_ localKey: String?)
    func queryAlbumInfo(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>?
    func queryAlbumCollection() -> NSFetchedResultsController<NSFetchRequestResult>?
    func checkAlbumInfoSaved(_ localKey: String?) -> Bool
}
