//
//  ServiceManager.swift
//  LastFm
//
//  Created by ktw on 19.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import CoreData

class ServiceManager {
    static let shared = ServiceManager(LocalService.shared, cacheService: CacheService.shared, networkService: NetworkService.shared)
    fileprivate let localService : LocalServiceProtocol!
    fileprivate let cacheService : CacheServiceProtocol!
    fileprivate let networkService : NetworkServiceProtocol!
    fileprivate var serviceHistory = [ServiceType : ServiceInfo]()
    
    private init(_ localService: LocalServiceProtocol, cacheService: CacheServiceProtocol, networkService: NetworkServiceProtocol) {
        self.localService = localService
        self.cacheService = cacheService
        self.networkService = networkService
        loadHistory()
    }
}


//
// MARK: - Service history
//
extension ServiceManager {
    
    func getServiceInfo(_ service: ServiceType) -> ServiceInfo? {
        var serviceInfo = serviceHistory[service]
        if nil == serviceInfo {
            let fetchedCtrl = makeFetchedResultsController(service)
            serviceInfo = ServiceInfo(0, fetchedResultsController: fetchedCtrl)
            serviceHistory[service] = serviceInfo
        }
        else if nil == serviceInfo?.fetchedResultsController {
            serviceInfo?.fetchedResultsController = makeFetchedResultsController(service)
        }
        
        return serviceInfo
    }
    
    func setServiceInfo(_ service: ServiceType, serviceInfo: ServiceInfo) {
        serviceHistory[service] = serviceInfo
    }
    
    func makeFetchedResultsController(_ service: ServiceType) -> NSFetchedResultsController<NSFetchRequestResult>? {
        switch service {
        case .artistList(let artistSearchKey): return cacheService.queryArtistList(artistSearchKey.localKey)
        case .albumList(let artistKey):        return cacheService.queryAlbumList(artistKey.localKey)
        case .albumInfo(let albumKey):         return cacheService.queryAlbumInfo(albumKey.localKey)
        }
    }
}


//
// MARK: - Service history memento(encoding/decoding) 
//
extension ServiceManager {
    private var documents: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    private enum Filenames {
        static let History = "history.json"
    }
    
    func saveHistory() {
        let url = documents.appendingPathComponent(Filenames.History)
        let encoder = JSONEncoder()
        guard let encodedData = try? encoder.encode(serviceHistory) else {
            return
        }
        try? encodedData.write(to: url)
    }
    
    func loadHistory() {
        let savedURL = documents.appendingPathComponent(Filenames.History)
        let data = try? Data(contentsOf: savedURL)
        if let historyData = data,
            let decodedHistory = try? JSONDecoder().decode([ServiceType : ServiceInfo].self, from: historyData) {
            serviceHistory = decodedHistory
        }
    }
}


//
// MARK: - ServiceProtocol
//
extension ServiceManager : ServiceProtocol {
    func queryService(_ service: ServiceType?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let service = service else {
            return nil
        }
        let serviceInfo = getServiceInfo(service)
        return serviceInfo?.fetchedResultsController
    }
    
    func requestService(_ service: ServiceType?, completionHandler: ((_ fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>)->Void)?) {
        guard let service = service else {
            return
        }
        switch service {
        case .artistList(let artistSearchKey): requestArtistList(service, withArtistSearchKey: artistSearchKey)
        case .albumList(let artistKey):        requestAlbumList(service, withArtistKey: artistKey)
        case .albumInfo(let albumKey):         requestAlbumInfo(service, withAlbumKey: albumKey, completionHandler: completionHandler)
        }
    }
    
    func emptyService(_ service: ServiceType?) {
        guard let service = service else {
            return
        }
        if let serviceInfo = getServiceInfo(service) {
            serviceInfo.page = 0
            cacheService.empty(serviceInfo.fetchedResultsController)
        }
    }
    
    
    func fetchAlbumInfo(_ albumKey: AlbumKey?) {
        guard let albumKey = albumKey else {
            return
        }
        
        let service = ServiceType.albumInfo(albumKey: albumKey)
        if let albumInfoFetchedCtrl = queryService(service),
            let albumInfo = albumInfoFetchedCtrl.fetchedObjects?.first as? AlbumInfoProtocol {
            saveAlbumInfo(albumInfo)
        }
        else {
            requestService(service) { albumInfoFetchedCtrl in
                if let albumInfo = albumInfoFetchedCtrl.fetchedObjects?.first as? AlbumInfoProtocol {
                    self.saveAlbumInfo(albumInfo)
                }
            }
        }
    }
    
    func saveAlbumInfo(_ albumInfo: AlbumInfoProtocol?) {
        localService.saveAlbumInfo(albumInfo)
    }
    
    func deleteAlbumInfo(_ localKey: String?) {
        localService.deleteAlbumInfo(localKey)
    }
    
    func queryAlbumInfo(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        return localService.queryAlbumInfo(localKey)
    }
    
    func queryAlbumCollection() -> NSFetchedResultsController<NSFetchRequestResult>? {
        return localService.queryAlbumCollection()
    }

    func checkAlbumInfoSaved(_ localKey: String?) -> Bool {
        return localService.checkAlbumInfoSaved(localKey)
    }
}



//
// MARK: - Request to NetworkService
//
extension ServiceManager {

    fileprivate func requestArtistList(_ service: ServiceType, withArtistSearchKey artistSearchKey: ArtistSearchKey) {
        guard let serviceInfo = getServiceInfo(service) else {
            return
        }
        
        let requestPage = serviceInfo.page + 1
        let netService = NetworkServiceType.artistList(artistSearchKey: artistSearchKey, page: requestPage) { artistList in
            if(0 < artistList.count) {
                serviceInfo.page = requestPage
                let idx = serviceInfo.fetchedResultsController?.fetchedObjects?.count
                self.cacheService.saveArtistList(artistSearchKey.localKey, index: idx, artists: artistList)
            }
        }
        let _ = networkService.request(netService)
    }
    
    fileprivate func requestAlbumList(_ service: ServiceType, withArtistKey artistKey: ArtistKey) {
        guard let serviceInfo = getServiceInfo(service) else {
            return
        }
        
        let requestPage = serviceInfo.page + 1
        let netService = NetworkServiceType.albumList(artistKey: artistKey, page: requestPage) { albumList in
            if(0 < albumList.count) {
                serviceInfo.page = requestPage
                let idx = serviceInfo.fetchedResultsController?.fetchedObjects?.count
                self.cacheService.saveAlbumList(artistKey.localKey, index: idx, albums: albumList)
            }
        }
        let _ = networkService.request(netService)
    }
    
    fileprivate func requestAlbumInfo(_ service: ServiceType, withAlbumKey albumKey: AlbumKey,
                                      completionHandler: ((_ fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>)->Void)?) {
        let netService = NetworkServiceType.albumInfo(albumKey: albumKey) { albumResponse in
            self.cacheService.saveAlbumInfo(albumKey.localKey, albumInfo: albumResponse)

            if let completionHandler = completionHandler,
                let fetchedReseltsController = self.cacheService.queryAlbumInfo(albumKey.localKey) {
                completionHandler(fetchedReseltsController)
            }
        }
        let _ = networkService.request(netService)
    }
}
