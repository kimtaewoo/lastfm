//
//  ArtistSearchKey.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

struct ArtistSearchKey : LocalKey, Hashable, Codable{
    var name: String?
}

extension ArtistSearchKey {
    var localKey: String {
        get {
            if let name = name {
                return name
            }
            else {
                return ""
            }
        }
    }
    
    var hashValue: Int {
        return localKey.hashValue
    }
    
    static func == (lhs: ArtistSearchKey, rhs: ArtistSearchKey) -> Bool {
        return lhs.name == rhs.name
    }
}
