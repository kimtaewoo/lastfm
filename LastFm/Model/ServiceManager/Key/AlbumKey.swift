//
//  AlbumKey.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

struct AlbumKey : LocalKey, Hashable, Codable{
    var mbid: String?
    var name: String?
    var artist: String?
}

extension AlbumKey {
    var localKey: String {
        get {
            if let mbid = mbid {
                return mbid
            }
            else if let name = name, let artist = artist {
                return name + artist
            }
            else {
                return ""
            }
        }
    }
    
    var hashValue: Int {
        return localKey.hashValue
    }
    
    static func == (lhs: AlbumKey, rhs: AlbumKey) -> Bool {
        return lhs.name == rhs.name && lhs.mbid == rhs.mbid && lhs.artist == rhs.artist
    }
}
