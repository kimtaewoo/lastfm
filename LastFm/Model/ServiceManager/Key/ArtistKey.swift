//
//  ArtistKey.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

struct ArtistKey : LocalKey, Hashable, Codable{
    var mbid: String?
    var name: String?
}

extension ArtistKey {
    var localKey: String {
        get {
            if let mbid = mbid {
                return mbid
            }
            else if let name = name {
                return name
            }
            else {
                return ""
            }
        }
    }
    
    var hashValue: Int {
        return localKey.hashValue
    }
    
    static func == (lhs: ArtistKey, rhs: ArtistKey) -> Bool {
        return lhs.name == rhs.name && lhs.mbid == rhs.mbid
    }
}

