//
//  CacheService.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CacheService {
    static let shared = CacheService()
    
    private init() {
        
    }
    
    static var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        
        let modelURL = Bundle(for: CacheService.self).url(forResource: "Cache", withExtension: "momd")! // type your database name here..
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let documentDirectory: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let url = documentDirectory?.appendingPathComponent("Cache.sqlite") // type your database name here...
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
            // try coordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: options)  // in-memory store
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    static var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    static func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

//
// MARK: - CacheServiceProtocol
//
extension CacheService : CacheServiceProtocol {
    
    func saveArtistList(_ localKey: String?, index: Int?, artists: [AnyObject]?) {
        guard let localKey = localKey,
            var index = index,
            let artists : [LFArtist] = artists as? [LFArtist] else {
            return
        }

        for artist in artists {
            let newArtist = NSEntityDescription.insertNewObject(forEntityName: "Artist", into: CacheService.managedObjectContext) as! CacheArtist
            newArtist.mbid = artist.mbid
            newArtist.name = artist.name
            let artistImage : LFArtistImage? = artist.images?.last
            newArtist.imageUrl = artistImage?.url
            newArtist.index = Int64(index)
            newArtist.localKey = localKey
            
            index += 1
            
            print("----------- CacheService, saved new artist")
        }
        
        CacheService.saveContext()
    }
    
    func saveAlbumList(_ localKey: String?, index: Int?, albums: [AnyObject]?) {
        guard let localKey = localKey,
            var index = index,
            let albums : [LFTopAlbum] = albums as? [LFTopAlbum] else {
            return
        }
        
        for album in albums {
            let newAlbum = NSEntityDescription.insertNewObject(forEntityName: "Album", into: CacheService.managedObjectContext) as! CacheAlbum
            newAlbum.mbid = album.mbid
            newAlbum.name = album.name
            newAlbum.artistName = album.artist?.name
            let albumImage : LFAlbumImage? = album.images?.last
            newAlbum.imageUrl = albumImage?.url
            newAlbum.index = Int64(index)
            newAlbum.localKey = localKey
            
            index += 1
            
            print("----------- CacheService, saved new album")
        }
        
        CacheService.saveContext()
    }
              
    
    func saveAlbumInfo(_ localKey: String?, albumInfo: AnyObject?) {
        guard let localKey = localKey,
            let albumInfo = albumInfo as? LFAlbumInfoResponse else {
            return
        }

        let newAlbumInfo = NSEntityDescription.insertNewObject(forEntityName: "AlbumInfo", into: CacheService.managedObjectContext) as! CacheAlbumInfo
        newAlbumInfo.mbid = albumInfo.mbid
        newAlbumInfo.name = albumInfo.name
        newAlbumInfo.artistName = albumInfo.artist
        let albumImage : LFAlbumImage? = albumInfo.images?.last
        newAlbumInfo.imageUrl = albumImage?.url
        newAlbumInfo.index = 0
        newAlbumInfo.localKey = localKey
        
        if let tracks = albumInfo.tracks?.tracks {
            for track in tracks {
                let newTrack = NSEntityDescription.insertNewObject(forEntityName: "Track", into: CacheService.managedObjectContext) as! CacheTrack
                newTrack.name = track.name
                newTrack.duration = track.duration
                newTrack.album = newAlbumInfo
            }
        }

        print("----------- CacheService, saved new album info")
        CacheService.saveContext()
    }


    func empty(_ fetchedController: NSFetchedResultsController<NSFetchRequestResult>?) {
        guard let fetchRequest = fetchedController?.fetchRequest else {
            return
        }
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try CacheService.managedObjectContext.execute(deleteRequest)
            try fetchedController?.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
//    func emptyArtistList(_ localKey: String?) {
//    }
//
//    func emptyAlbumList(_ localKey: String?) {
//        guard let localKey = localKey else {
//            return
//        }
//
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Album")
//        fetchRequest.predicate = NSPredicate(format: "localKey == %@", localKey)
//        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//        do {
//            let _ = try CacheService.persistentStoreCoordinator.execute(deleteRequest, with: CacheService.managedObjectContext)
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        }
//    }
//
//
//    func emptyAlbumInfo(_ localKey: String?) {
//        guard let localKey = localKey else {
//            return
//        }
//
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
//        fetchRequest.predicate = NSPredicate(format: "localKey == %@", localKey)
//        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//        do {
//            let _ = try CacheService.persistentStoreCoordinator.execute(deleteRequest, with: CacheService.managedObjectContext)
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        }
//    }
    
    
    func queryArtistList(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let localKey = localKey else {
            return nil
        }
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Artist")
        fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CacheService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        // Configure Fetched Results Controller
        return fetchedResultsController
    }
    
    func queryAlbumList(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let localKey = localKey else {
            return nil
        }
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Album")
        fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CacheService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        // Configure Fetched Results Controller
        return fetchedResultsController
    }
    
    
    func queryAlbumInfo(_ localKey: String?) -> NSFetchedResultsController<NSFetchRequestResult>? {
        guard let localKey = localKey else {
            return nil
        }
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AlbumInfo")
        fetchRequest.predicate = NSPredicate.init(format: "localKey == %@", localKey)
        
        // Add Sort Descriptors
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CacheService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        // Configure Fetched Results Controller
        return fetchedResultsController
    }
}





