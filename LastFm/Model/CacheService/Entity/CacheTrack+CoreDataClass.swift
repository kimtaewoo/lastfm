//
//  CacheTrack+CoreDataClass.swift
//  LastFm
//
//  Created by ktw on 31.10.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData


public class CacheTrack: NSManagedObject {

}

extension CacheTrack: TrackProtocol {
    var albumInfo : AlbumInfoProtocol? {
        get {
            return album
        }
    }
}
