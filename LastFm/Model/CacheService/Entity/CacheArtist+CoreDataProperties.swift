//
//  CacheArtist+CoreDataProperties.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData


extension CacheArtist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CacheArtist> {
        return NSFetchRequest<CacheArtist>(entityName: "Artist")
    }

    @NSManaged public var imageUrl: String?
    @NSManaged public var index: Int64
    @NSManaged public var localKey: String?
    @NSManaged public var mbid: String?
    @NSManaged public var name: String?

}
