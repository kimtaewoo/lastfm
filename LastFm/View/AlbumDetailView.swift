//
//  AlbumDetailTableViewCell
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import AlamofireImage

protocol AlbumDetailViewDelegate: class {
    func moveToPrevAlbumDetail()
    func moveToNextAlbumDetail()
}

class AlbumDetailView: UIView {
    struct ViewModel {
        let name: String?
        let artistName: String?
        let imageUrl: URL?
    }
    
    @IBOutlet weak var imageViewAlbum: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var imageViewRight: UIImageView!
    weak var delegate: AlbumDetailViewDelegate?

    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelName.text = viewModel.name
            labelArtist.text = viewModel.artistName
            if let url = viewModel.imageUrl {
                imageViewAlbum.af_setImage(withURL: url, placeholderImage: UIImage(named: "signia-343"))
            }
            else {
                imageViewAlbum.image = UIImage(named: "signia-343")!
            }
            
            disappearIcons()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onTouchUp_prev(_ sender: UIButton) {
        delegate?.moveToPrevAlbumDetail()
    }
    
    @IBAction func onTouchUp_next(_ sender: UIButton) {
        delegate?.moveToNextAlbumDetail()
    }
}

extension AlbumDetailView {
    func disappearIcons() {
        self.imageViewLeft?.isHidden = false;
        self.imageViewRight?.isHidden = false;
        
        let when = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.imageViewLeft?.isHidden = true;
            self.imageViewRight?.isHidden = true;
            
            let transition = CATransition()
            transition.type = kCATransitionFade
            self.layer.add(transition, forKey: nil)
        }
    }
}

extension AlbumDetailView.ViewModel {
    init(_ album: AlbumInfoProtocol) {
        name = album.name
        artistName = album.artistName
        imageUrl = URL.init(string: album.imageUrl!)
    }
    
    init() {
        name = ""
        artistName = ""
        imageUrl = nil
    }
    
}
