//
//  AlbumTrackTableViewCell
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import AlamofireImage

class AlbumTrackTableViewCell: UITableViewCell {
    struct ViewModel {
        let name: String?
        let duration: String?
    }
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelName.text = viewModel.name
            labelDuration.text = viewModel.duration?.formatted
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


extension AlbumTrackTableViewCell.ViewModel {
    init(_ trackList: TrackProtocol) {
        name = trackList.name
        duration = trackList.duration
    }

    init() {
        name = ""
        duration = ""
    }
}


extension String {
    var formatted: String {
        guard let sec = Int(self) else {
            return ""
        }
        
        let (h, m, s) = (sec / 3600, (sec % 3600) / 60, (sec % 3600) % 60)
        if 0 < h {
            return ("\(h):\(m):\(s)")
        }
        else {
            return ("\(m):\(s)")
        }            
    }
}
