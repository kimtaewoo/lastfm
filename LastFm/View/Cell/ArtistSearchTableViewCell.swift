//
//  ArtistSearchTableViewCell.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import AlamofireImage

class ArtistSearchTableViewCell: UITableViewCell {
    struct ViewModel {
		let name: String?
        let imageUrl: URL?
	}
    
    @IBOutlet weak var imageViewArtist: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelName.text = viewModel.name
            if let url = viewModel.imageUrl {
                imageViewArtist.af_setImage(withURL: url, placeholderImage: UIImage(named: "signia-197"))
            }
            else {
                imageViewArtist.image = UIImage(named: "signia-197")!
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ArtistSearchTableViewCell.ViewModel {
    init(_ artist: ArtistProtocol) {
        name = artist.name
        imageUrl = URL.init(string: artist.imageUrl!)
    }

    init() {
        name = ""
        imageUrl = nil
    }
}
