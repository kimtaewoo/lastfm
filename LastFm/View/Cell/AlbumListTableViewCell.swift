//
//  AlbumListTableViewCell
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit
import AlamofireImage

class AlbumListTableViewCell: UITableViewCell {
    struct ViewModel {
        let name: String?
        let artistName: String?
        let imageUrl: URL?
        let localKey: String?
        var isSaved: Bool
    }
    
    @IBOutlet weak var imageViewAlbum: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var buttonSave: UIButton!
    var saveAction: (()->Void)?
    var deleteAction: (()->Void)?

    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelName.text = viewModel.name
            labelArtist.text = viewModel.artistName
            if let url = viewModel.imageUrl {
                imageViewAlbum.af_setImage(withURL: url, placeholderImage: UIImage(named: "signia-343"))
            }
            else {
                imageViewAlbum.image = UIImage(named: "signia-343")!
            }
            buttonSave.isSelected = viewModel.isSaved
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onTouchUp_save(_ sender: Any) {
        if buttonSave.isSelected {
            deleteAction?()
        }
        else {
            saveAction?()
        }
    }
    
}


extension AlbumListTableViewCell.ViewModel {
    init(_ album: AlbumProtocol, saved: Bool) {
        name = album.name
        artistName = album.artistName
        imageUrl = URL.init(string: album.imageUrl!)
        localKey = album.localKey
        isSaved = saved
    }
    
    init() {
        name = nil
        artistName = nil
        imageUrl = nil
        localKey = nil
        isSaved = false
    }
    
}
