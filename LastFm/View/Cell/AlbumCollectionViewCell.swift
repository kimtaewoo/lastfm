//
//  AlbumCollectionViewCell.swift
//  LastFm
//
//  Created by ktw on 20.09.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    struct ViewModel {
        let name: String?
        let imageUrl: URL?
    }
    
    @IBOutlet weak var imageViewAlbum: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelName.text = viewModel.name
            if let url = viewModel.imageUrl {
                imageViewAlbum.af_setImage(withURL: url, placeholderImage: UIImage(named: "signia-343"))
            }
            else {
                imageViewAlbum.image = UIImage(named: "signia-343")!
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}


extension AlbumCollectionViewCell.ViewModel {
    init(_ albumInfo: AlbumInfoProtocol) {
        name = albumInfo.name
        imageUrl = URL.init(string: albumInfo.imageUrl!)
    }
    
    init() {
        name = ""
        imageUrl = nil
    }
}
