//
//  IndexPath+Extension.swift
//  LastFm
//
//  Created by ktw on 01.11.17.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

extension IndexPath {
    
    func prev() -> IndexPath? {
        if(0 < self.row) {
            return IndexPath(row: self.row - 1, section: self.section)
        }
        else {
            return nil
        }
    }
    
    func next(lessThan max: Int) -> IndexPath? {
        if self.row < max - 1 {
            return IndexPath(row: self.row + 1, section: self.section)
        }
        else {
            return nil
        }
    }
}
